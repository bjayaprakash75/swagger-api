import { Operation } from "express-openapi";
 

  export const parameters = [
    {
      in: 'path',
      name: 'id',
      required: true,
      type: 'integer'
    }
   ];
   
  export const GET: Operation = [
      /* business middleware not expressible by OpenAPI documentation goes here */
      (req, res, next) => {
          res.status(200).json();
      }
  ];
   
  const apiDoc = {
    description: 'A description for retrieving a user.',
    tags: ['users'],
    operationId: 'getUser',
    // parameters for this operation
    parameters: [
      {
        in: 'query',
        name: 'firstName',
        type: 'string'
      }
    ],
    responses: {
      default: {
        $ref: '#/definitions/Error'
      }
    }
  };
   
  export const POST: Operation = (req, res, next) => {
      /* ... */
  }
   
  POST.apiDoc = {
      /* ... */
  }; 