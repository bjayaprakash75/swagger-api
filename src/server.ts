import * as express from 'express';
import * as bodyParser from 'body-parser';
import * as mongoose from 'mongoose';
import { Routes } from './routes/routes';
import * as cors from 'cors';
import { WinstonLogger } from './config/Winstonlogger';
import { MongoConfig } from './config/MongoConfig';
import * as path from 'path';
import { initialize } from "express-openapi";
import { AdminServcie } from './api-doc';
import * as swaggerUi from 'swagger-ui-express';
import * as openapidoc from './routes/swagger.json';
import * as YAML from 'yaml';


const PORT = 3000;

class App {

    public app = express();
    public routerPrv: Routes = new Routes();
    public logger: WinstonLogger = new WinstonLogger();
    public admin: AdminServcie = new AdminServcie();
    
    
    constructor() {
        this.logger.setupLogger();
        this.logger.configureWinston(this.app);
        this.initializeMiddlewares();
        this.routerPrv.routes(this.app);
    }

    private initializeMiddlewares() {
        this.app.use(bodyParser.json());
        this.app.use(bodyParser.urlencoded({ extended: false }));
        this.app.use(cors({ credentials: true, origin: true }));
        initialize({
            apiDoc: this.admin.apiDoc,
            app: this.app,
            validateApiDoc: false,
            routesGlob: '**/*.{ts,js}',
            routesIndexFileRegExp: /(?:index)?\.[tj]s$/,
            paths: path.resolve(
                __dirname,
                'built',
                'api-paths',
                'users'
              ),
        });
        this.app.use(((err, req, res, next) => {
            res.status(err.status).json(err);
        }) as express.ErrorRequestHandler);
        this.app.use('/swagger', swaggerUi.serve, swaggerUi.setup(openapidoc));
        // this.app.use('/swagger123', swaggerUi.serve, swaggerUi.setup(YAML('./routes/openapi')));
        
    }



}

new App().app.listen(PORT, () => {
    console.log('Express server listening on port ' + PORT);
})
