import { AdminController } from '../controllers/adminController';
import { AdminFrontendController } from '../controllers/adminFrontendController';
import { AdminServcie } from '../api-doc';


const adminControler = new AdminController();
const adminFrontendController = new AdminFrontendController();
const adminservicce = new AdminServcie();
var options = {explorer: true};
const admin = new AdminServcie();

export class Routes {

    public routes(app): void {
        app.route('/execute').get(adminControler.execute);
        app.route('/hello').post(adminFrontendController.hello);
    }

}