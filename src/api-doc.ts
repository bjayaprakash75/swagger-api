

export class AdminServcie {
    public apiDoc = {
    swagger: '2.0',
    basePath: '/',
    info: {
      title: 'A getting started API.',
      description: "This is a sample server Petstore server.  You can find out more about     Swagger at [http://swagger.io](http://swagger.io) or on [irc.freenode.net, #swagger](http://swagger.io/irc/).      For this sample, you can use the api key `special-key` to test the authorization     filters.",
      version: "1.0.0",
      termsOfService: "http://swagger.io/terms/",
      contact: {
        email: "apiteam@swagger.io"},
      license:{
        name: "Apache 2.0",
        url: "http://www.apache.org/licenses/LICENSE-2.0.html"
      },
    },
    definitions: {
      World: {
        type: 'object',
        properties: {
          name: {
            description: 'The name of this world.',
            type: 'string'
          }
        },
        required: ['name']
      }
    },
    paths: {}
  };
  public openApiDoc = {
    openapi: '3.0.1',
    basePath: '/',
    info: {
      version: '1.3.0',
      title: 'Users',
      description: 'User management API',
      termsOfService: 'http://api_url/terms/',
      contact: {
        name: 'Wolox Team',
        email: 'hello@wolox.co',
        url: 'https://www.wolox.com.ar/'
      },
      license: {
        name: 'Apache 2.0',
        url: 'https://www.apache.org/licenses/LICENSE-2.0.html'
      }
    },
    host: 'http://localhost:3000',
    servers: [
      {
        url: 'http://localhost:3000/',
        description: 'Local server'
      },
      {
        url: 'https://api_url_testing',
        description: 'Testing server'
      },
      {
        url: 'https://api_url_production',
        description: 'Production server'
      }
    ],
    tags: [
      {
        name: 'CRUD operations'
      }
    ],
    paths: {
      '/users': {
        get: {
          tags: ['CRUD operations'],
          description: 'Get users',
          operationId: 'getUsers',
          parameters: [
            {
              name: 'x-company-id',
              in: 'header',
              schema: {
                $ref: '#/components/schemas/companyId'
              },
              required: true,
              description: 'Company id where the users work'
            },
            {
              name: 'page',
              in: 'query',
              schema: {
                type: 'integer',
                default: 1
              },
              required: false
            },
            {
              name: 'orderBy',
              in: 'query',
              schema: {
                type: 'string',
                enum: ['asc', 'desc'],
                default: 'asc'
              },
              required: false
            }
          ],
          responses: {
            '200': {
              description: 'Users were obtained',
              content: {
                'application/json': {
                  schema: {
                    $ref: '#/components/schemas/Users'
                  }
                }
              }
            },
            '400': {
              description: 'Missing parameters',
              content: {
                'application/json': {
                  schema: {
                    $ref: '#/components/schemas/Error'
                  },
                  example: {
                    message: 'companyId is missing',
                    internal_code: 'missing_parameters'
                  }
                }
              }
            }
          }
        }
      }
    },
    components: {
      schemas: {
        identificationNumber: {
          type: 'integer',
          description: 'User identification number',
          example: 1234
        },
        username: {
          type: 'string',
          example: 'raparicio'
        },
        userType: {
          type: 'string',
          enum: 'USER_TYPES',
          default: 'REGULAR'
        },
        companyId: {
          type: 'integer',
          description: 'Company id where the user works',
          example: 15
        },
        User: {
          type: 'object',
          properties: {
            identificationNumber: {
              $ref: '#/components/schemas/identificationNumber'
            },
            username: {
              $ref: '#/components/schemas/username'
            },
            userType: {
              $ref: '#/components/schemas/userType'
            },
            companyId: {
              $ref: '#/components/schemas/companyId'
            }
          }
        },
        Users: {
          type: 'object',
          properties: {
            users: {
              type: 'array',
              items: {
                $ref: '#/components/schemas/User'
              }
            }
          }
        },
        Error: {
          type: 'object',
          properties: {
            message: {
              type: 'string'
            },
            internal_code: {
              type: 'string'
            }
          }
        }
      }
    }
    /* ... */
  }
}